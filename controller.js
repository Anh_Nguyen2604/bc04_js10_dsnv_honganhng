function layThongTinTuForm() {
  const stKhoan = document.getElementById("tknv").value;
  const tenNv = document.getElementById("name").value;
  const tkEmail = document.getElementById("email").value;
  const pass = document.getElementById("password").value;
  const ngayLam = document.getElementById("datepicker").value;
  const luongCoBan = document.getElementById("luongCB").value;
  const postition = document.getElementById("chucvu").value;
  const hour = document.getElementById("gioLam").value;

  return new NhanVien(
    stKhoan,
    tenNv,
    tkEmail,
    pass,
    ngayLam,
    luongCoBan,
    postition,
    hour
  );
}

function renderDSNV(dsnv) {
  console.log("sn2", dsnv);
  var contentHTML = "";
  dsnv.forEach((nv) => {
    var contentTr = `<tr>
        <td>${nv.taiKhoan}</td>
        <td>${nv.ten}</td>
        <td>${nv.email}</td>
         
        <td>${nv.date}</td>
        <td>${nv.chucVu}</td>
        <td>${nv.tinhTongLuong()}</td>
        <td>${nv.xepLoaiNV()}</td>
        <td>
        <button onclick="xoaNhanVien('${nv.taiKhoan}')" 
        class="btn btn-danger">Xoa
        </button>
        <button onclick="suaNhanVien('${nv.taiKhoan}')" 
        class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sua
        </button>        
        </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(taiKhoan, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.taiKhoan == taiKhoan) {
      return index;
    }
  }
  return -1;
}
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.date;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
