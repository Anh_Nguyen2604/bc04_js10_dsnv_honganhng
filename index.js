const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";
var dsnv = [];

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matKhau,
      nv.date,
      nv.luong,
      nv.chucVu,
      nv.gioLam
    );
  }

  renderDSNV(dsnv);
}

function themNhanVien() {
  var newNV = layThongTinTuForm();

  var isValid =
    validation.kiemTraRong(newNV.taiKhoan, "tbTKNV", "TKNV k dc de trong") &&
    validation.kiemTraDoDai(
      newNV.taiKhoan,
      "tbTKNV",
      "TKN phai tu 4-6 ki tu",
      4,
      6
    );

  isValid =
    isValid &
      validation.kiemTraRong(newNV.ten, "tbTen", "Ten NV ko dc de rong") &&
    validation.kiemTraChu(newNV.ten, "tbTen", "Ten NV kphai la chu");

  isValid =
    isValid &
      validation.kiemTraRong(newNV.email, "tbEmail", "Email ko dc de rong") &&
    validation.kiemTraEmail(newNV.email, "tbEmail", "Email khong dung format");

  isValid =
    isValid &
      validation.kiemTraRong(newNV.matKhau, "tbMatKhau", "MK ko dc de rong") &&
    validation.kiemTraDoDai(
      newNV.matKhau,
      "tbMatKhau",
      "MK phai tu 4-10 ki tu",
      4,
      10
    ) &&
    validation.kiemTraMatKhau(
      newNV.matKhau,
      "tbMatKhau",
      "MK phai chua 1 chu hoa, 1 chu thuong, 1 so, 1 ki tu dac biet"
    );

  isValid =
    isValid &
      validation.kiemTraRong(
        newNV.luong,
        "tbLuongCB",
        "luong CB ko dc de trong"
      ) &&
    validation.kiemTraGiaTri(
      newNV.luong,
      "tbLuongCB",
      "Luong khong hop le",
      1000000,
      20000000
    );

  isValid =
    isValid &
    validation.kiemTraChucvu(newNV.chucVu, "tbChucVu", "chuc vu ko hop le");

  isValid =
    isValid &
      validation.kiemTraRong(newNV.gioLam, "tbGiolam", "gio ko dc de rong") &&
    validation.kiemTraSo(newNV.gioLam, "tbGiolam", "gio phai la so") &&
    validation.kiemTraGiaTri(
      newNV.gioLam,
      "tbGiolam",
      "gio phai tu 80-20 gio",
      80,
      200
    );
  if (isValid) {
    dsnv.push(newNV);
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
  }
}

function xoaNhanVien(taiKhoan) {
  console.log(taiKhoan);

  var index = timKiemViTri(taiKhoan, dsnv);

  if (index != -1) {
    dsnv.splice(index, 1);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
  }
}

function suaNhanVien(taikhoan) {
  console.log(taikhoan);

  var index = timKiemViTri(taikhoan, dsnv);

  if (index != -1) {
    var nv = dsnv[index];
    showThongTinLenForm(nv);
  }
}

function updateNhanVien(taiKhoan) {
  var taiKhoan = document.getElementById("tknv").value;
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.taiKhoan == taiKhoan && kiemtraValidator()) {
      dsnv[index] = layThongTinTuForm();
      break;

      /*var index = timKiemViTri(taiKhoan, dsnv);
    console.log("index: ", index);
    if (index != -1) {
      var nv = dsnv[index];
      showThongTinLenForm(nv);
    }*/
    }
  }

  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
  renderDSNV(dsnv);
}

function xepLoaiNhanVien() {
  var xepLoai = document.getElementById("searchName").value;
  var xuatSac = ["xuat sac"];
  var gioi = ["gioi"];
  var kha = ["kha"];
  var trungBinh = ["trung binh"];

  var xepLoaiArr = [xuatSac, gioi, kha, trungBinh];
  for (index = 0; index < xepLoaiArr.length; index++) {
    if (xepLoaiArr[index].indexOf(xepLoai) != -1) {
      xepLoai = xepLoaiArr[index][0];
    }
  }
}

renderDSNV(dsnv);
