function NhanVien(taiKhoan, ten, email, matKhau, date, luong, chucVu, gioLam) {
  this.taiKhoan = taiKhoan;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.date = date;
  this.luong = luong;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tinhTongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luong * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luong * 2;
    } else if (this.chucVu == "Nhân viên") {
      return this.luong * 1;
    }
  };

  this.xepLoaiNV = function () {
    if (gioLam >= 192) {
      return "Nhan vien xuat sac";
    } else if (gioLam >= 176) {
      return "Nhan vien gioi";
    } else if (gioLam >= 160) {
      return "Nhan vien kha";
    } else {
      return "Nhan vien trung binh";
    }
  };
}
